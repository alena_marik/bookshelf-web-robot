package org.am.bookshelfwebrobot.web;

import org.am.bookshelfwebrobot.businesslogic.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * http://localhost:8081/api/data/author
 */
@RestController
@RequestMapping("/api/data/author")
public class AuthorCtrl {

    @Autowired
    private AuthorService authorService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity getAuthors() {
        return new ResponseEntity(authorService.getAll(), HttpStatus.OK);
    }

}
