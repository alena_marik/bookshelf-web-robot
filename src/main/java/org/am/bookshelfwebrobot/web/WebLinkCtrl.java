package org.am.bookshelfwebrobot.web;

import org.am.bookshelfwebrobot.businesslogic.service.WebLinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * http://localhost:8081/api/data/web-link
 */
@RestController
@RequestMapping("/api/data/web-link")
public class WebLinkCtrl {

    @Autowired
    private WebLinkService webLinkService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity getWebLinks(){
        return new ResponseEntity(webLinkService.getAll(), HttpStatus.OK);
    }


}
