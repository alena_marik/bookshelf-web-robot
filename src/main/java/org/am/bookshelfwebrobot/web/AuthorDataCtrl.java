package org.am.bookshelfwebrobot.web;

import org.am.bookshelfwebrobot.businesslogic.service.AuthorDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/data/author-data")
public class AuthorDataCtrl {

    @Autowired
    private AuthorDataService authorDataService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity getAuthorData(){
        return new ResponseEntity(authorDataService.getALl(), HttpStatus.OK);
    }



}
