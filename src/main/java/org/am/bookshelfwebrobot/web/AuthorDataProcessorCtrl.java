package org.am.bookshelfwebrobot.web;

import org.am.bookshelfwebrobot.businesslogic.authordataprocessor.AuthorDataProcessor;
import org.am.bookshelfwebrobot.businesslogic.authordataprocessor.DataProcessing;
import org.am.bookshelfwebrobot.entity.WebSite;
import org.am.bookshelfwebrobot.util.ProcessOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * http://localhost:8081/api/processor/author-data
 */
@RestController
@RequestMapping("/api/processor/author-data")
public class AuthorDataProcessorCtrl {

    private Logger logger = LoggerFactory.getLogger(AuthorDataProcessorCtrl.class);

    @Autowired
    private AuthorDataProcessor authorDataProcessor;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity processAuthorData(@RequestParam(value = "process-option", required = false) ProcessOption processOption) {
        authorDataProcessor.process();
        return new ResponseEntity(HttpStatus.OK);
    }

}
