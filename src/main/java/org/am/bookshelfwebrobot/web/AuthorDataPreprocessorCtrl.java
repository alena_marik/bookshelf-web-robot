package org.am.bookshelfwebrobot.web;

import org.am.bookshelfwebrobot.businesslogic.authordatapreprocessor.AuthorDataPreprocessor;
import org.am.bookshelfwebrobot.entity.WebSite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * http://localhost:8081/api/preprocessor/author-data
 */
@RestController
@RequestMapping("/api/preprocessor/author-data")
public class AuthorDataPreprocessorCtrl {

    @Autowired
    private AuthorDataPreprocessor authorDataPreprocessor;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public void preprocessAuthorData(@RequestParam(value = "website", required = false) WebSite webSite) {
        preprocessAuthorData(webSite);
    }

}
