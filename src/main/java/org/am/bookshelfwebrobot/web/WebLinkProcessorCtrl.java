package org.am.bookshelfwebrobot.web;

import org.am.bookshelfwebrobot.businesslogic.weblinkprocessor.WebLinkProcessor;
import org.am.bookshelfwebrobot.repository.WebLinkRepository;
import org.am.bookshelfwebrobot.util.ProcessOption;
import org.am.bookshelfwebrobot.businesslogic.weblinkprocessor.BookDBWebLinkProcessor;
import org.am.bookshelfwebrobot.businesslogic.weblinkprocessor.CbdbWebLinkProcessor;
import org.am.bookshelfwebrobot.businesslogic.weblinkprocessor.WikiWebLinkProcessor;
import org.am.bookshelfwebrobot.entity.WebSite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * http://localhost:8081/api/processor/web-link
 * http://localhost:8081/api/processor/web-link?website=CBDB
 * http://localhost:8081/api/processor/web-link?website=WIKIPEIDA
 * http://localhost:8081/api/processor/web-link?website=BOOK_DATABASE
 */
@RestController
@RequestMapping("/api/processor/web-link")
public class WebLinkProcessorCtrl {

    private Logger logger = LoggerFactory.getLogger(WebPageCrawlerCtrl.class);

    @Autowired
    private WebLinkProcessor webLinkProcessor;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity processWebLinks(@RequestParam(value = "website", required = false) WebSite webSite, @RequestParam(value = "process-option", required = false) ProcessOption option) {
        if (option == null){
            option = ProcessOption.CONTINUE;
        }
        webLinkProcessor.process(webSite, option);
        return new ResponseEntity(HttpStatus.OK);
    }
}
