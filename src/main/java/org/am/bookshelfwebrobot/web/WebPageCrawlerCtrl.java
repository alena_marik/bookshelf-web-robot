package org.am.bookshelfwebrobot.web;

import org.am.bookshelfwebrobot.entity.Author;
import org.am.bookshelfwebrobot.util.ProcessOption;
import org.am.bookshelfwebrobot.businesslogic.webcrawler.BookDBWebCrawler;
import org.am.bookshelfwebrobot.businesslogic.webcrawler.CbdbWebCrawler;
import org.am.bookshelfwebrobot.businesslogic.webcrawler.WikiWebCrawler;
import org.am.bookshelfwebrobot.entity.WebSite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * http://localhost:8081/api/web-crawler?website=CBDB&process-option=START
 * http://localhost:8081/api/web-crawler?website=BOOK_DATABASE&process-option=START
 * http://localhost:8081/api/web-crawler?website=WIKIPEDIA&process-option=START
 */
@RestController
@RequestMapping("/api/web-crawler")
public class WebPageCrawlerCtrl {

    private Logger logger = LoggerFactory.getLogger(WebPageCrawlerCtrl.class);

    @Autowired
    private BookDBWebCrawler bookDBCrawler;

    @Autowired
    private WikiWebCrawler wikiCrawler;

    @Autowired
    private CbdbWebCrawler cbdbCrawler;


    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public ResponseEntity test() {
       return new ResponseEntity(new Author(), HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity crawlWebPage(@RequestParam(value = "website") WebSite webSite, @RequestParam(value = "process-option", required = false) ProcessOption option) {
        if (option == null){
            option = ProcessOption.CONTINUE;
        }
        switch (webSite) {
            case CBDB:
                 cbdbCrawler.process(option);
                break;
            case BOOK_DATABASE:
                bookDBCrawler.process(option);
                break;
            case WIKIPEDIA:
                wikiCrawler.process(option);
                break;
            default:
                logger.error("Illegal option for website: " + webSite);
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

}
