package org.am.bookshelfwebrobot.businesslogic.weblinkprocessor;

import org.am.bookshelfwebrobot.entity.AuthorData;
import org.am.bookshelfwebrobot.entity.State;
import org.am.bookshelfwebrobot.entity.WebLink;
import org.am.bookshelfwebrobot.entity.WebSite;
import org.am.bookshelfwebrobot.repository.AuthorDataRepository;
import org.am.bookshelfwebrobot.repository.WebLinkRepository;
import org.am.bookshelfwebrobot.util.ProcessOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Service
public class WebLinkProcessor {

    private Logger logger = LoggerFactory.getLogger(WebLinkProcessor.class);

    @Autowired
    private WebLinkRepository webLinkRepository;

    @Autowired
    private AuthorDataRepository authorDataRepository;

    @Autowired
    private CbdbWebLinkProcessor cbdbWebLinkProcessor;

    @Autowired
    private BookDBWebLinkProcessor bookDBWebLinkProcessor;

    @Autowired
    private WikiWebLinkProcessor wikiWebLinkProcessor;

    public void process(WebSite webSite, ProcessOption option) {
        Set<String> authorDataLinks = initAuthorDataLinks(webSite);
        List<WebLink> webLinks = initWebLinkSet(webSite, option);
        switch (option) {
            case START:
                processStart(authorDataLinks, webLinks);
            case UPDATE:
                processUpdate(authorDataLinks, webLinks);
            case CONTINUE:
                processContinue(authorDataLinks, webLinks);
            default:
                logger.error("Illegal option for ProcessOption: " + option);
                throw new RuntimeException();
        }

    }

    private Set<String> initAuthorDataLinks(WebSite webSite) {
        return authorDataRepository.findWebLinks(webSite);
    }

    private List<WebLink> initWebLinkSet(WebSite webSite, ProcessOption option) {
        switch (option) {
            case START:
            case UPDATE:
                return webSite == null ? webLinkRepository.findAll() : webLinkRepository.findByWebSite(webSite);
            case CONTINUE:
                return webSite == null ? webLinkRepository.findByState(State.UNPROCESSED) : webLinkRepository.findByWebSiteAndState(webSite, State.UNPROCESSED);
            default:
                logger.error("Illegal option for ProcessOption: " + option);
                throw new RuntimeException();
        }
    }

    private void processStart(Set<String> authorDataLinks, List<WebLink> webLinks) {
        for (WebLink link : webLinks) {
            String url = link.getUrl();
            if (authorDataLinks.contains(url)) {
                AuthorData authorData = authorDataRepository.findByUrl(url);
                authorDataRepository.delete(authorData);
            }
            AuthorData authorData = new AuthorData();
            parseWebLinkToAuthorData(link, authorData);
            saveAuthorAndWebLink(authorData, link);
        }
    }

    private void processUpdate(Set<String> authorDataLinks, List<WebLink> webLinks) {
        for (WebLink link : webLinks) {
            String url = link.getUrl();
            AuthorData authorData = authorDataLinks.contains(url) ? authorDataRepository.findByUrl(url) : new AuthorData();
            parseWebLinkToAuthorData(link, authorData);
            saveAuthorAndWebLink(authorData, link);
        }
    }

    private void processContinue(Set<String> authorDataLinks, List<WebLink> webLinks) {
        for (WebLink link : webLinks) {
            if (authorDataLinks.contains(link.getUrl())) {
                continue;
            }
            AuthorData authorData = new AuthorData();
            parseWebLinkToAuthorData(link, authorData);
            saveAuthorAndWebLink(authorData, link);
        }
    }


    private AuthorData parseWebLinkToAuthorData(WebLink webLink, @NotNull AuthorData authorData) {
        WebSite webSite = webLink.getWebSite();
        switch (webSite) {
            case BOOK_DATABASE:
                bookDBWebLinkProcessor.processWebLink(authorData, webLink.getUrl());
                break;
            case CBDB:
                cbdbWebLinkProcessor.processWebLink(authorData, webLink.getUrl());
                break;
            case WIKIPEDIA:
                wikiWebLinkProcessor.processWebLink(authorData, webLink.getUrl());
                break;
            default:
                logger.error("Illegal option for WebSite: " + webSite);
                throw new RuntimeException();
        }
        mapWebLinkToAuthorData(webLink, authorData);
        return authorData;
    }


    @Transactional
    protected void saveAuthorAndWebLink(AuthorData authorData, WebLink webLink) {
        authorDataRepository.saveAndFlush(authorData);
        webLinkRepository.saveAndFlush(webLink);
    }

    private static void mapWebLinkToAuthorData(WebLink webLink, AuthorData authorData) {
        webLink.setState(State.PROCESSED);
        authorData.setState(State.UNPROCESSED);

        authorData.setName(webLink.getAuthorName());
        authorData.setUrl(webLink.getUrl());
        authorData.setWebSite(webLink.getWebSite());
        authorData.setAdditionalInfo(webLink.getAdditionalInfo());
    }
}
