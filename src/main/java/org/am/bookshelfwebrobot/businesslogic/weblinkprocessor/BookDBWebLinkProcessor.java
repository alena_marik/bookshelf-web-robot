package org.am.bookshelfwebrobot.businesslogic.weblinkprocessor;

import org.am.bookshelfwebrobot.entity.*;
import org.am.bookshelfwebrobot.util.JsoupUtil;
import org.am.bookshelfwebrobot.util.ProcessOption;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

@Service
public class BookDBWebLinkProcessor{

    protected AuthorData processWebLink(AuthorData authorData, String url) {
        Document document = JsoupUtil.getDocument(url);

        //TODO books

        //TODO biography

        return authorData;
    }

}
