package org.am.bookshelfwebrobot.businesslogic.weblinkprocessor;

import org.am.bookshelfwebrobot.entity.*;
import org.am.bookshelfwebrobot.util.JsoupUtil;
import org.am.bookshelfwebrobot.util.ProcessOption;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

@Service
public class WikiWebLinkProcessor {

    protected AuthorData processWebLink(AuthorData authorData, String url) {
        final Document document = JsoupUtil.getDocument(url);

        // TODO get author info from the page

        return authorData;
    }

}
