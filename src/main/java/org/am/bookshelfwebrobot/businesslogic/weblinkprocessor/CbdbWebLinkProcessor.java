package org.am.bookshelfwebrobot.businesslogic.weblinkprocessor;

import org.am.bookshelfwebrobot.entity.AuthorData;
import org.am.bookshelfwebrobot.entity.State;
import org.am.bookshelfwebrobot.entity.WebSite;
import org.am.bookshelfwebrobot.util.JsoupUtil;
import org.am.bookshelfwebrobot.util.ProcessOption;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CbdbWebLinkProcessor {

    private Logger logger = LoggerFactory.getLogger(CbdbWebLinkProcessor.class);

    protected AuthorData processWebLink(AuthorData authorData, String url) {
        Document document = JsoupUtil.getDocument(url);

        Elements nameElement = document.select("span[itemprop = name]");
        authorData.setName(nameElement.text());

        Elements birthDate = document.select("span[itemprop = birthDate]");
        authorData.setBirthTime(birthDate.text());

        Elements deathDate = document.select("span[itemprop = deathDate]");
        authorData.setDeathTime(deathDate.text());

        Elements birthPlace = document.select("span[itemprop = birthPlace]");
        authorData.setBirthPlace(birthPlace.text());

        //TODO: books from page

        return authorData;
    }
}
