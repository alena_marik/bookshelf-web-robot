package org.am.bookshelfwebrobot.businesslogic.authordataprocessor;

public enum DataProcessing {

    PREPROCESS, PAIR, PROCESS;

}
