package org.am.bookshelfwebrobot.businesslogic.authordataprocessor;

import org.am.bookshelfwebrobot.entity.Author;
import org.am.bookshelfwebrobot.entity.AuthorData;
import org.am.bookshelfwebrobot.entity.State;
import org.am.bookshelfwebrobot.repository.AuthorDataRepository;
import org.am.bookshelfwebrobot.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorDataProcessor {

    @Autowired
    private AuthorDataRepository authorDataRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private WikiAuthorDataProcessor wikiAuthorDataProcessor;

    @Autowired
    private BookDBAuthorDataProcessor bookDBAuthorDataProcessor;

    @Autowired
    private CbdbAuthorDataProcessor cbdbAuthorDataProcessor;

    @Async
    public void process() {
        List<AuthorData> authorDatas = authorDataRepository.findByState(State.PREPROCESSED);
        for(AuthorData data : authorDatas){
            if (data.getPairingId() != null){
                authorDataRepository.findByPairingId(data.getPairingId());
            }
            Author author = new Author();
        }
    }

    private void  (AuthorData data, Author author){

    }

    private void mapAuthorDataToAuthor(List<AuthorData> data, Author author){

    }

}
