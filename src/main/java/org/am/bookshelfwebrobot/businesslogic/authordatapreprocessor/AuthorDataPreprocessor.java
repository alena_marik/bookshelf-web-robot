package org.am.bookshelfwebrobot.businesslogic.authordatapreprocessor;

import org.am.bookshelfwebrobot.entity.AuthorData;
import org.am.bookshelfwebrobot.entity.State;
import org.am.bookshelfwebrobot.entity.WebSite;
import org.am.bookshelfwebrobot.repository.AuthorDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;

@Service
public class AuthorDataPreprocessor {

    @Autowired
    protected AuthorDataRepository authorDataRepository;

    @Autowired
    private WikiAuthorDataPreprocessor wikiAuthorDataPreprocessor;

    @Autowired
    private BookDBAuthorDataPreprocessor bookDBAuthorDataPreprocessor;

    @Autowired
    private CbdbAuthorDataPreprocessor cbdbAuthorDataPreprocessor;

    @Async
    public void preprocess() {
        List<AuthorData> unprocessedAuthorData = authorDataRepository.findByState(State.UNPROCESSED);
        preprocess(unprocessedAuthorData);
    }

    @Async
    public void preprocess(WebSite webSite) {
        List<AuthorData> unprocessedAuthorData = authorDataRepository.findByWebSiteAndState(webSite, State.UNPROCESSED);
        preprocess(unprocessedAuthorData);
    }

    @Async
    public void preprocess(List<AuthorData> unprocessedAuthorData) {
        Iterator<AuthorData> iterator = unprocessedAuthorData.iterator();
        while (iterator.hasNext()) {
            AuthorData data = iterator.next();
            preprocessAuthorData(data);
            iterator.remove();
        }
    }

    private void preprocessAuthorData(AuthorData data) {
        switch (data.getWebSite()) {
            case BOOK_DATABASE:
                bookDBAuthorDataPreprocessor.preprocessAuthorData(data);
                break;
            case WIKIPEDIA:
                wikiAuthorDataPreprocessor.preprocessAuthorData(data);
                break;
            case CBDB:
                cbdbAuthorDataPreprocessor.preprocessAuthorData(data);
                break;
            default:
                break;
        }
        data.setState(State.PREPROCESSED);
        authorDataRepository.saveAndFlush(data);
    }

    @Transactional
    public void pair() {
        List<AuthorData> notPairedData = authorDataRepository.findByWebSiteAndNullPairingId(WebSite.WIKIPEDIA);
        for (AuthorData data : notPairedData) {
            String lastName = data.getLastName();
            List<AuthorData> cbdbAuthorData = authorDataRepository.findByLastNameAndWebSite(lastName, WebSite.CBDB);
            Long maxPairingId = authorDataRepository.findMaxPairingId();
            for (AuthorData cbdbData : cbdbAuthorData){
               if (compareAuthorData(data, cbdbData)){

                   data.setPairingId(maxPairingId);
                   cbdbData.setPairingId(maxPairingId);
                   authorDataRepository.saveAndFlush(data);
                   authorDataRepository.saveAndFlush(cbdbData);
               }
            }
            List<AuthorData> bookDBAuthorData = authorDataRepository.findByLastNameAndWebSite(lastName, WebSite.BOOK_DATABASE);
            for (AuthorData bookDBData : bookDBAuthorData){
                if (compareAuthorData(data, bookDBData)){
                    data.setPairingId(maxPairingId);
                    bookDBData.setPairingId(maxPairingId);
                    authorDataRepository.saveAndFlush(data);
                    authorDataRepository.saveAndFlush(bookDBData);
                }
            }
        }
    }

    private boolean compareAuthorData(AuthorData a, AuthorData b){
        boolean lastNameMatch = compareLastNames(a.getLastName(), b.getLastName());
        boolean firstNameMatch = compareFirstNames(a.getFirstName(), b.getLastName());
        boolean birthYearMatch = compareYears(a.getBirthYear(), b.getBirthYear());
        boolean birthDateMatch = compareDates(a.getBirthDate(), b.getBirthDate());
        boolean deathYearMatch = compareYears(a.getDeathYear(), b.getDeathYear());
        boolean deathDateMatch = compareDates(a.getDeathDate(), b.getDeathDate());
        return lastNameMatch && firstNameMatch && birthYearMatch && deathYearMatch && birthDateMatch && deathDateMatch;
    }

    private boolean compareLastNames(String lastNameA, String lastNameB){
        if (lastNameA == null || lastNameB == null){
            return true;
        }
        String optionA = lastNameA.toLowerCase();
        String optionB = lastNameB.toLowerCase();
        if (optionA.contains(optionB) || optionB.contains(optionA)){
            return true;
        }

        return false;
    }

    private boolean compareFirstNames(String firstNameA, String firstNameB){
        if (firstNameA == null || firstNameB == null){
            return true;
        }
        String optionA = firstNameA.toLowerCase();
        String optionB = firstNameB.toLowerCase();
        if (optionA.contains(optionB) || optionB.contains(optionA)){
            return true;
        }
        return false;
    }

    private boolean compareYears(Integer yearA, Integer yearB){
        if (yearA == null || yearB == null){
            return true;
        }
        if (yearA.equals(yearB)){
            return true;
        }
        if (Math.abs(yearA - yearB) <=  2){
            return true;
        }
        return false;
    }

    private boolean compareDates(LocalDate dateA, LocalDate dateB){
        if (dateA == null || dateB == null){
            return true;
        }
        if (dateA.equals(dateB)){
            return true;
        }
        return false;
    }

}
