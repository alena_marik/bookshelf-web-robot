package org.am.bookshelfwebrobot.businesslogic.authordatapreprocessor;

import org.am.bookshelfwebrobot.entity.AuthorData;
import org.am.bookshelfwebrobot.util.DateUtil;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.stream.Collectors;

@Service
public class CbdbAuthorDataPreprocessor {


    private static final String unknownDate = "??.??.";

    protected void preprocessAuthorData(AuthorData data) {
        setNames(data.getName(), data);
        setCountry(data);
        setDate(DateUtil.DateType.BIRTH_DATE, data, data.getBirthTime());
        setDate(DateUtil.DateType.DEATH_DATE, data, data.getDeathTime());
    }

    private static void setNames(@NonNull String name, AuthorData authorData) {
        String[] split = name.split(" ");
        int length = split.length;
        if (length == 1) {
            authorData.setLastName(split[0].trim());
        } else if (length == 2) {
            authorData.setFirstName(split[0].trim());
            authorData.setLastName(split[1].trim());
        } else if (length > 2){
            authorData.setLastName(split[length - 1].trim());
            authorData.setFirstName(Arrays.stream(Arrays.copyOfRange(split, 0, length - 2)).collect(Collectors.joining(" ")));
        }
    }

    private static void setCountry(AuthorData data) {
        String birthPlace = data.getBirthPlace();
        if (birthPlace != null && !birthPlace.isEmpty()) {
            String[] split = birthPlace.split(",");
            data.setCountry(split[split.length - 1].trim());
        }
    }

    private static void setDate(DateUtil.DateType dateType, AuthorData authorData, String date) {
        if (date == null) {
            return;
        }
        LocalDate localDate = parseCbdbDate(date);
        if (localDate != null) {
            DateUtil.setAuthorDate(dateType, authorData, localDate);
            DateUtil.setAuthorDate(dateType, authorData, localDate.getYear());
        } else {
            if (date.contains(unknownDate)) {
                date = date.replace(unknownDate, "");
            }

            Integer year = parseCbdbYear(date);
            DateUtil.setAuthorDate(dateType, authorData, year);
        }
    }

    private static LocalDate parseCbdbDate(String date) {
        try {
            return LocalDate.parse(date, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        } catch (Exception e) {
            return null;
        }
    }

    private static Integer parseCbdbYear(String date) {
        try {
            return Integer.parseInt(date);
        } catch (Exception e) {
            return null;
        }
    }
}
