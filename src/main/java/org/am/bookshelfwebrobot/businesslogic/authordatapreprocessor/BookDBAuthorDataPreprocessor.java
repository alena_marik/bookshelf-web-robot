package org.am.bookshelfwebrobot.businesslogic.authordatapreprocessor;

import org.am.bookshelfwebrobot.entity.AuthorData;
import org.springframework.stereotype.Service;

@Service
public class BookDBAuthorDataPreprocessor {

    protected void preprocessAuthorData(AuthorData data) {
        setNames(data.getName(), data);

    }

    private static void setNames(String name, AuthorData authorData) {
        String[] split = name.split(",");
        if (split.length >= 1) {
            authorData.setLastName(split[0].trim());
        }
        if (split.length == 2) {
            authorData.setFirstName(split[1].trim());
        }
    }

}
