package org.am.bookshelfwebrobot.businesslogic.webcrawler;

import org.am.bookshelfwebrobot.entity.WebCrawlerState;
import org.am.bookshelfwebrobot.entity.WebSite;
import org.am.bookshelfwebrobot.util.JsoupUtil;
import org.am.bookshelfwebrobot.util.ProcessOption;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CbdbWebCrawler extends WebCrawler {

    private Logger logger = LoggerFactory.getLogger(CbdbWebCrawler.class);

    private static final String CDBD = "https://www.cbdb.cz/";
    private static final String CDBD_BOOKS = "https://www.cbdb.cz/knihy";
    private static final String CDBD_AUTHORS = "https://www.cbdb.cz/spisovatele";

    @Override
    public void afterPropertiesSet() {
        existingRecords = webLinkRepository.getWebLinks(WebSite.CBDB);
    }

    @Override
    protected void processStart() {
        processOption = ProcessOption.START;
        getAuthorLinksFromWebsite('a', 'z', 1);
    }

    @Override
    protected void processUpdate() {
        processOption = ProcessOption.UPDATE;
        getAuthorLinksFromWebsite('a', 'z', 1);
    }

    @Override
    protected void processContinue() {
        processOption = ProcessOption.CONTINUE;
        WebCrawlerState state = webCrawlerStateService.getOne(WebSite.CBDB);
        getAuthorLinksFromWebsite(state.getLetter(), 'z', state.getPageNumber());
    }

    private void getAuthorLinksFromWebsite(char startChar, char endChar, int initPageCount) {
        for (char c = startChar; c <= endChar; c++) {
            int pageCount = c == startChar ? initPageCount : 1;
            getAuthorLinksFromWebsite(c, pageCount);
        }
    }

    private void getAuthorLinksFromWebsite(char letter, int pageCount) {

        String url = CDBD_AUTHORS + "-" + letter;

        Elements linksOnPage = getAuthorLinksFromPage(url + "-" + pageCount);
        while (linksOnPage.size() > 0) {
            getAuthorLinksFromSinglePage(linksOnPage);

            webCrawlerStateService.updateCbdbWebCrawlState(url + "-" + pageCount);
            linksOnPage = getAuthorLinksFromPage(url + "-" + ++pageCount);
        }
    }

    private Elements getAuthorLinksFromPage(String url) {
        return JsoupUtil.getDocument(url).select("table.textlist").select("a[href]");
    }

    private void getAuthorLinksFromSinglePage(Elements linksOnPage) {
        for (Element element : linksOnPage) {
            generateWebLinks(element);
        }
    }

    @Transactional
    private void generateWebLinks(Element element) {
        String hrefAttribute = element.attr("abs:href");
        Elements ratingElement = null;
        if (element.parent().parent().select("td.textlist_rating_0").size() == 0) {
            if (element.parent().parent().select("td.textlist_rating_1").size() == 0) {
                ratingElement = element.parent().parent().select("td.textlist_rating_2");
            } else {
                ratingElement = element.parent().parent().select("td.textlist_rating_1");
            }
        } else {
            ratingElement = element.parent().parent().select("td.textlist_rating_0");
        }
        if (!existingRecords.contains(hrefAttribute)) {
            saveWebLink(hrefAttribute, element.text(), ratingElement.text(), WebSite.CBDB);
            existingRecords.add(hrefAttribute);
        }
    }

}
