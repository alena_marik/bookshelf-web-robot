package org.am.bookshelfwebrobot.businesslogic.webcrawler;

import org.am.bookshelfwebrobot.businesslogic.service.AuthorService;
import org.am.bookshelfwebrobot.businesslogic.service.WebCrawlerStateService;
import org.am.bookshelfwebrobot.entity.State;
import org.am.bookshelfwebrobot.entity.WebLink;
import org.am.bookshelfwebrobot.entity.WebSite;
import org.am.bookshelfwebrobot.repository.WebLinkRepository;
import org.am.bookshelfwebrobot.util.ProcessOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public abstract class WebCrawler implements InitializingBean {

    private Logger logger = LoggerFactory.getLogger(WebCrawler.class);

    protected ProcessOption processOption = ProcessOption.CONTINUE;

    protected Set<String> existingRecords;

    @Autowired
    protected AuthorService authorService;

    @Autowired
    protected WebLinkRepository webLinkRepository;

    @Autowired
    protected WebCrawlerStateService webCrawlerStateService;

    @Async
    public void process(ProcessOption option) {
        switch (option) {
            case START:
                processStart();
                break;
            case CONTINUE:
                processContinue();
                break;
            default:
                logger.error("Illegal option for ProcessOption: " + option);
                break;
        }
    }

    /**
     * Fully processing authors from given website - getting all links from page, mapping document to AuthorData and saving to the database
     */
    protected abstract void processStart();

    /**
     * Processing authors from given website - getting all links from page, mapping document to AuthorData and saving to the database
     */
    protected abstract void processUpdate();

    /**
     * Processing authors from given website - getting all links from page, mapping document to AuthorData and saving to the database
     */
    protected abstract void processContinue();

    private WebLink createWebLink(String url, String name, String additionalInfo, WebSite webSite){
        WebLink webLink = new WebLink();
        webLink.setState(State.UNPROCESSED);
        webLink.setUrl(url);
        webLink.setWebSite(webSite);
        webLink.setAuthorName(name);
        webLink.setAdditionalInfo(additionalInfo);
        return webLink;
    }

    protected void saveWebLink(String url, String name, String additionalInfo, WebSite webSite){
      switch (processOption){
          case START:
              saveWebLinkStart(url, name, additionalInfo, webSite);
              break;
          case CONTINUE:
              saveWebLinkContinue(url, name, additionalInfo, webSite);
              break;
          default:
              logger.error("Illegal option for process option " + processOption);
      }
    }

    private void saveWebLinkStart(String url, String name, String additionalInfo, WebSite webSite){
        if (existingRecords.contains(url)){
            WebLink oldRecord = webLinkRepository.getOne(url);
            webLinkRepository.delete(oldRecord);
        }
        WebLink newRecord = createWebLink(url, name, additionalInfo, webSite);
        webLinkRepository.saveAndFlush(newRecord);
    }

    private void saveWebLinkContinue(String url, String name, String additionalInfo, WebSite webSite){
        if (existingRecords.contains(url)){
            return;
        }
        WebLink webLink = createWebLink(url, name, additionalInfo, webSite);
        webLinkRepository.saveAndFlush(webLink);
    }



}
