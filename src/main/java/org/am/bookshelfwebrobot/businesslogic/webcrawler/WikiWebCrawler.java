package org.am.bookshelfwebrobot.businesslogic.webcrawler;

import org.am.bookshelfwebrobot.entity.WebCrawlerState;
import org.am.bookshelfwebrobot.entity.WebSite;
import org.am.bookshelfwebrobot.util.JsoupUtil;
import org.am.bookshelfwebrobot.util.ProcessOption;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class WikiWebCrawler extends WebCrawler {

    private Logger logger = LoggerFactory.getLogger(WikiWebCrawler.class);

    private static final String WIKIPEDIA_AUTHORS = "https://en.wikipedia.org/wiki/Lists_of_writers";
    private static final String WIKIPEDIA_AUTHORS_BY_ALPHABET = "https://en.wikipedia.org/wiki/List_of_authors_by_name:_";
    private static final String REGEX = "https:\\/\\/en.wikipedia.org\\/wiki\\/[a-zA-Z]*(_[a-zA-Z]*)*$";

    @Override
    public void afterPropertiesSet() {
        existingRecords = authorService.getWikiWebLinks();
    }

    @Override
    protected void processStart() {
        processOption = ProcessOption.START;
        getAuthorLinksFromWebsite('A', 'Z');
    }

    @Override
    protected void processUpdate() {
        processOption = ProcessOption.UPDATE;
        getAuthorLinksFromWebsite('A', 'Z');
    }

    @Override
    protected void processContinue() {
        processOption = ProcessOption.CONTINUE;
        WebCrawlerState state = webCrawlerStateService.getOne(WebSite.WIKIPEDIA);
        getAuthorLinksFromWebsite(state.getLetter(), 'Z');
    }

    private void getAuthorLinksFromWebsite(char startChar, char endChar) {
        for (char c = startChar; c <= endChar; c++) {
            getAuthorLinksFromSinglePage(c);
            webCrawlerStateService.updateWikiWebCrawlState(WIKIPEDIA_AUTHORS_BY_ALPHABET + c);
        }
    }

    private void getAuthorLinksFromSinglePage(char letter) {
        Document document = JsoupUtil.getDocument(WIKIPEDIA_AUTHORS_BY_ALPHABET + letter);
        Elements links = JsoupUtil.getHrefElementsFromPage(document);
        for (Element element : links) {
            String url = element.attr("abs:href");
            if (url == null) {
                continue;
            }
            if (document != null && url.matches(REGEX) && !url.contains("List")
                    && !url.contains("poet") && !url.contains("Main_Page") && !url.contains("oetr")) {
                List<Node> nodes = element.parent().childNodes();
                String additionalInfo = null;
                if (nodes.size() == 2 && nodes.get(1) instanceof TextNode) {
                    additionalInfo = ((TextNode) nodes.get(1)).getWholeText();
                }
                generateWebLinks(url, element.text(), additionalInfo);
            }
        }
    }

    @Transactional
    private void generateWebLinks(String hrefAttribute, String name, String additionalInfo) {
        if (!existingRecords.contains(hrefAttribute)) {
            saveWebLink(hrefAttribute, name, additionalInfo, WebSite.WIKIPEDIA);
            existingRecords.add(hrefAttribute);
        }
    }

}
