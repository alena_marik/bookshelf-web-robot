package org.am.bookshelfwebrobot.businesslogic.webcrawler;

import org.am.bookshelfwebrobot.entity.WebCrawlerState;
import org.am.bookshelfwebrobot.entity.WebSite;
import org.am.bookshelfwebrobot.util.JsoupUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
public class BookDBWebCrawler extends WebCrawler {

    private static final String DATABASE_BOOK_AUTHOR_ALPHABET = "https://www.databazeknih.cz/autori?nid=&dle=az&id=";
    private static final String DATABASE_BOOK_AUTHOR_ALPHABET_RO = "https://www.databazeknih.cz/autori?nid=&dle=za&id=";
    private static final String DATABASE_BOOK_AUTHOR_YEAR_BORN = "https://www.databazeknih.cz/autori?nid=&dle=rna&id=";
    private static final String DATABASE_BOOK_AUTHOR_YEAR_BORN_RO = "https://www.databazeknih.cz/autori?nid=&dle=rnz&id=";
    private static final String DATABASE_BOOK_AUTHOR_POPULARITY = "https://www.databazeknih.cz/autori?nid=&dle=rat&id=";

    private static List<String> DATABASE_BOOK_AUTHOR_URL;

    static {
        DATABASE_BOOK_AUTHOR_URL = new LinkedList<>();
        DATABASE_BOOK_AUTHOR_URL.add(DATABASE_BOOK_AUTHOR_ALPHABET);
        DATABASE_BOOK_AUTHOR_URL.add(DATABASE_BOOK_AUTHOR_ALPHABET_RO);
        DATABASE_BOOK_AUTHOR_URL.add(DATABASE_BOOK_AUTHOR_YEAR_BORN);
        DATABASE_BOOK_AUTHOR_URL.add(DATABASE_BOOK_AUTHOR_YEAR_BORN_RO);
        DATABASE_BOOK_AUTHOR_URL.add(DATABASE_BOOK_AUTHOR_POPULARITY);
    }

    @Override
    public void afterPropertiesSet() {
        existingRecords = webLinkRepository.getWebLinks(WebSite.BOOK_DATABASE);
    }

    @Override
    protected void processStart() {
        getAuthorLinksFromWebsite(DATABASE_BOOK_AUTHOR_URL.get(0), 1);
    }

    @Override
    protected void processUpdate() {
        getAuthorLinksFromWebsite(DATABASE_BOOK_AUTHOR_URL.get(0), 1);
    }

    @Override
    protected void processContinue() {
        WebCrawlerState state = webCrawlerStateService.getOne(WebSite.BOOK_DATABASE);
        getAuthorLinksFromWebsite(state.getSorting(), state.getPageNumber());
    }

    private void getAuthorLinksFromWebsite(String sorting, int pageCount) {
        for (int i = DATABASE_BOOK_AUTHOR_URL.indexOf(sorting); i < DATABASE_BOOK_AUTHOR_URL.size(); i++) {
            String url = DATABASE_BOOK_AUTHOR_URL.get(i);
            int pageCount1 = url.equals(sorting) ? pageCount : 1;
            getAuthorLinksFromSortedWebsite(url, pageCount1);
        }
    }

    private void getAuthorLinksFromSortedWebsite(String url, int pageCount) {
        Document document = JsoupUtil.getDocument(url + pageCount);
        while (!document.text().contains("Více stran nelze zobrazit.")) {
            getAuthorLinksFromSinglePage(document);
            webCrawlerStateService.updateBookDBWebCrawlState(url + pageCount);
            document = JsoupUtil.getDocument(url + ++pageCount);
        }
    }

    @Transactional
    private void getAuthorLinksFromSinglePage(Document document) {
        Elements linksOnPage = document.select("div.autbox").select("a[href]");
        for (Element element : linksOnPage) {
            String url = element.attr("abs:href");
            List<Node> nodes = element.parent().childNodes();
            String additionalInfo = null;
            if (nodes.size() == 3) {
                additionalInfo = ((Element) element.parent().childNodes().get(2)).select("span.pozn_light").text();
            }
            if (url.contains("autori/") && !existingRecords.contains(url)) {
                saveWebLink(url, element.text(), additionalInfo, WebSite.BOOK_DATABASE);
                existingRecords.add(url);
            }
        }
    }


}
