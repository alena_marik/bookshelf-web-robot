package org.am.bookshelfwebrobot.businesslogic.service;

import org.am.bookshelfwebrobot.entity.WebLink;
import org.am.bookshelfwebrobot.repository.WebLinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WebLinkService {

    @Autowired
    private WebLinkRepository webLinkRepository;

    public List<WebLink> getAll(){
        return webLinkRepository.findAll();
    }

}
