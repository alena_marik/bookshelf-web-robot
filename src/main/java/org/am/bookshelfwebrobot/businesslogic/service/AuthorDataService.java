package org.am.bookshelfwebrobot.businesslogic.service;

import org.am.bookshelfwebrobot.entity.AuthorData;
import org.am.bookshelfwebrobot.repository.AuthorDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorDataService {

    @Autowired
    private AuthorDataRepository authorDataRepository;

    public List<AuthorData> getALl(){
        return authorDataRepository.findAll();
    }

}
