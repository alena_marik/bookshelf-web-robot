package org.am.bookshelfwebrobot.businesslogic.service;

import org.am.bookshelfwebrobot.entity.Author;
import org.am.bookshelfwebrobot.entity.AuthorData;
import org.am.bookshelfwebrobot.entity.WebLink;
import org.am.bookshelfwebrobot.repository.AuthorDataRepository;
import org.am.bookshelfwebrobot.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private AuthorDataRepository authorDataRepository;

    public List<Author> getAll(){
        return authorRepository.findAll();
    }

    public void saveCbdbAuthors(List<WebLink> webLinks) {

    }

    public void saveCbdbAuthor(AuthorData authorData) {

    }

    public void saveBookDBAuthors(List<AuthorData> authorData) {
        for (AuthorData author : authorData) {
            saveBookDBAuthor(author);
        }
    }

    public void saveBookDBAuthor(AuthorData authorData) {
        authorDataRepository.saveAndFlush(authorData);

    }

    public void saveWikiAuthors(List<AuthorData> authorData) {
        for (AuthorData author : authorData) {
            saveWikiAuthor(author);
        }
    }

    public void saveWikiAuthor(AuthorData authorData) {

    }

    public Set<String> getCbdbWebLinks() {
        return authorDataRepository.findCbdbWebLink();
    }

    public Set<String> getBookDBWebLinks() {
        return authorDataRepository.findBookDBWebLink();
    }

    public Set<String> getWikiWebLinks() {
        return authorDataRepository.findWikiWebLink();
    }

}
