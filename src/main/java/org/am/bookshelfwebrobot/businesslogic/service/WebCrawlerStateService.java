package org.am.bookshelfwebrobot.businesslogic.service;

import org.am.bookshelfwebrobot.entity.WebCrawlerState;
import org.am.bookshelfwebrobot.entity.WebSite;
import org.am.bookshelfwebrobot.repository.WebCrawlerStateRepository;
import org.am.bookshelfwebrobot.repository.WebLinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class WebCrawlerStateService {

    @Autowired
    private WebCrawlerStateRepository crawlerStateRepository;

    @Autowired
    private WebLinkRepository webLinkRepository;

    public WebCrawlerState getOne(WebSite webSite){
        return crawlerStateRepository.getOne(webSite.getId());
    }

    public void updateWebCrawlState(WebSite webSite, String url){
        switch (webSite){
            case CBDB:
                updateCbdbWebCrawlState(url);
                break;
            case WIKIPEDIA:
                updateWikiWebCrawlState(url);
            case BOOK_DATABASE:
                updateBookDBWebCrawlState(url);
        }
    }

    @Transactional
    public void updateCbdbWebCrawlState(String url){
        WebCrawlerState state = crawlerStateRepository.getOne(WebSite.CBDB.getId());
        if (state == null){
            state = new WebCrawlerState();
            state.setWebSite(WebSite.CBDB);
        }
        String[] split = url.split("-");
        if (split.length == 3 && "https://www.cbdb.cz/spisovatele".equals(split[0])){
            state.setLetter(split[1].charAt(0));
            state.setPageNumber(Integer.parseInt(split[2]));
        }
        state.setUrl(url);
        crawlerStateRepository.saveAndFlush(state);
    }

    public void updateBookDBWebCrawlState(String url){
        WebCrawlerState state = crawlerStateRepository.getOne(WebSite.BOOK_DATABASE.getId());
        if (state == null){
            state = new WebCrawlerState();
            state.setWebSite(WebSite.BOOK_DATABASE);
        }
        //https://www.databazeknih.cz/autori?nid=&dle=az&id=1
        String[] split = url.split("=");
        state.setUrl(url);
        state.setPageNumber(Integer.parseInt(split[split.length-1]));
        state.setSorting(url.replace(split[split.length-1],""));
        crawlerStateRepository.saveAndFlush(state);
    }

    public void updateWikiWebCrawlState(String url){
        WebCrawlerState state = crawlerStateRepository.getOne(WebSite.WIKIPEDIA.getId());
        if (state == null){
            state = new WebCrawlerState();
            state.setWebSite(WebSite.WIKIPEDIA);
        }
        String[] split = url.split("_");
        if (split.length == 6){
            state.setLetter(split[5].charAt(0));
        }
        state.setUrl(url);
    }

}
