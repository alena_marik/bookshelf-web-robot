package org.am.bookshelfwebrobot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.Async;

@Async
@SpringBootApplication
public class BookshelfWebRobotApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookshelfWebRobotApplication.class, args);
	}
}
