package org.am.bookshelfwebrobot.util;

import org.am.bookshelfwebrobot.entity.AuthorData;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtil {

    public enum DateType {
        BIRTH_DATE, DEATH_DATE
    }

    private DateUtil() {
    }

    public static AuthorData setAuthorDate(DateType dateType, AuthorData authorData, LocalDate date) {
        switch (dateType) {
            case BIRTH_DATE:
                authorData.setBirthDate(date);
                break;
            case DEATH_DATE:
                authorData.setDeathDate(date);
                break;
        }
        return authorData;
    }

    public static AuthorData setAuthorDate(DateType dateType, AuthorData authorData, Integer year) {
        switch (dateType) {
            case BIRTH_DATE:
                authorData.setBirthYear(year);
                break;
            case DEATH_DATE:
                authorData.setDeathYear(year);
                break;
        }
        return authorData;
    }

}
