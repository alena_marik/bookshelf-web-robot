package org.am.bookshelfwebrobot.util;

public enum ProcessOption {

    START, CONTINUE, UPDATE

}
