package org.am.bookshelfwebrobot.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class JsoupUtil {

    private static Logger logger = LoggerFactory.getLogger(JsoupUtil.class);

    private JsoupUtil(){}

    public static Document getDocument(String url) {
        try {
            Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
            return doc;
        } catch (IOException e){
            logger.error("Error getting document from url: " + url + ". Full error message: " + e.getMessage());

        }
        return null;
    }

    public static Elements getHrefElementsFromPage(Element element){
        return element.select("a[href]");
    }

}
