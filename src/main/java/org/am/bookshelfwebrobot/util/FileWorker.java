package org.am.bookshelfwebrobot.util;

import org.am.bookshelfwebrobot.entity.AuthorData;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

public class FileWorker {


    public static void createFile(List<String> data) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Java Books");

        int rowCount = 0;


        for (String link : data) {
            Row row = sheet.createRow(++rowCount);

            Cell cell = row.createCell(0);
            cell.setCellValue(link);
        }

        try (FileOutputStream outputStream = new FileOutputStream("Authors" + System.currentTimeMillis() + ".xlsx")) {
            workbook.write(outputStream);
        }
    }

    public static void createAuthorFile(List<AuthorData> data) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Java Books");

        int rowCount = 0;


        for (AuthorData authorData : data) {
            Row row = sheet.createRow(++rowCount);
            int cellCount = 0;

            Cell nameCell = row.createCell(cellCount++);
            nameCell.setCellValue(authorData.getName());

            Cell firstNameCell = row.createCell(cellCount++);
            firstNameCell.setCellValue(authorData.getFirstName());

            Cell lastNameCell = row.createCell(cellCount++);
            lastNameCell.setCellValue(authorData.getLastName());

            Cell birthTimeCell = row.createCell(cellCount++);
            if(authorData.getBirthTime() != null) {
                birthTimeCell.setCellValue(authorData.getBirthTime());
            }
            Cell birthDateCell = row.createCell(cellCount++);
            if (authorData.getBirthDate() != null) {
                birthDateCell.setCellValue(authorData.getBirthDate().format(DateTimeFormatter.ISO_DATE));
            }
            Cell birthYearCell = row.createCell(cellCount++);
            if (authorData.getBirthYear() != null) {
                birthYearCell.setCellValue(authorData.getBirthYear());
            }

            Cell deathTimeCell = row.createCell(cellCount++);
            if (authorData.getDeathTime() != null) {
                deathTimeCell.setCellValue(authorData.getDeathTime());

            }

            Cell deathDateCell = row.createCell(cellCount++);
            if (authorData.getDeathDate() != null) {
                deathDateCell.setCellValue(authorData.getDeathDate().format(DateTimeFormatter.ISO_DATE));
            }

            Cell deathYearCell = row.createCell(cellCount++);
            if (authorData.getDeathYear() != null) {
                deathYearCell.setCellValue(authorData.getDeathYear());
            }

            Cell birthPlaceCell = row.createCell(cellCount++);
            if (authorData.getBirthPlace() != null) {
                birthPlaceCell.setCellValue(authorData.getBirthPlace());
            }

            Cell webLinkCell = row.createCell(cellCount++);
            webLinkCell.setCellValue(authorData.getUrl());
        }

        try (FileOutputStream outputStream = new FileOutputStream("Authors" + System.currentTimeMillis() + ".xlsx")) {
            workbook.write(outputStream);
        }
    }

    public static void createFile(Map<String, String> data) throws IOException{
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Java Books");

        int rowCount = 0;

        for (Map.Entry<String, String> entry : data.entrySet()){
            Row row = sheet.createRow(++rowCount);

            Cell nameCell = row.createCell(1);
            nameCell.setCellValue(entry.getKey());
            Cell urlCell = row.createCell(2);
            urlCell.setCellValue(entry.getValue());
        }

        try (FileOutputStream outputStream = new FileOutputStream("Authors" + System.currentTimeMillis() + ".xlsx")) {
            workbook.write(outputStream);
        }
    }


    public static void createFile(Object[][] data) throws IOException{
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Java Books");

        int rowCount = 0;


        for (Object[] aBook : data) {
            Row row = sheet.createRow(++rowCount);

            int columnCount = 0;

            for (Object field : aBook) {
                Cell cell = row.createCell(++columnCount);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                }
            }

        }
        try (FileOutputStream outputStream = new FileOutputStream("Authors" + System.currentTimeMillis() + ".xlsx")) {
            workbook.write(outputStream);
        }
    }



}
