package org.am.bookshelfwebrobot.entity;

public enum State{

    UNPROCESSED, PREPROCESSED, PROCESSED

}

