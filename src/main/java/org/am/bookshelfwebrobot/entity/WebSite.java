package org.am.bookshelfwebrobot.entity;

public enum WebSite{

    BOOK_DATABASE(1), CBDB(2), WIKIPEDIA(3);

    private int id;

    WebSite(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }

}