package org.am.bookshelfwebrobot.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = AuthorData.TABLE_NAME)
public class AuthorData extends BSWEntity {

    private final static String AUTHOR_DATA = "AUTHOR_DATA";
    protected final static String TABLE_NAME = BSW + AUTHOR_DATA;
    private final static String SEQUENCE_NAME = SEQ + TABLE_NAME;

    @SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    @Id
    @Column(name="ID")
    private Long id;

    @Column(name="NAME")
    private String name;

    @Column(name="FIRST_NAME")
    private String firstName;

    @Column(name="LAST_NAME")
    private String lastName;

    @Column(name="BIRTH_TIME")
    private String birthTime;

    @Column(name="BIRTH_DATE")
    private LocalDate birthDate;

    @Column(name="BIRTH_YEAR")
    private Integer birthYear;

    @Column(name="DEATH_TIME")
    private String deathTime;

    @Column(name="DEATH_DATE")
    private LocalDate deathDate;

    @Column(name="DEATH_YEAR")
    private Integer deathYear;

    @Column(name="BIRTH_PLACE")
    private String birthPlace;

    @Column(name="COUNTRY")
    private String country;

    @Column(name="URL")
    private String url;

    @Column(name = "WEB_SITE")
    @Enumerated(EnumType.STRING)
    private WebSite webSite;

    @Column(name = "ADDITIONAL_INFO")
    private String additionalInfo;

    @Column(name = "PAIRING_ID")
    private Long pairingId;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private State state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthTime() {
        return birthTime;
    }

    public void setBirthTime(String birthTime) {
        this.birthTime = birthTime;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(Integer birthYear) {
        this.birthYear = birthYear;
    }

    public String getDeathTime() {
        return deathTime;
    }

    public void setDeathTime(String deathTime) {
        this.deathTime = deathTime;
    }

    public LocalDate getDeathDate() {
        return deathDate;
    }

    public void setDeathDate(LocalDate deathDate) {
        this.deathDate = deathDate;
    }

    public Integer getDeathYear() {
        return deathYear;
    }

    public void setDeathYear(Integer deathYear) {
        this.deathYear = deathYear;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public WebSite getWebSite() {
        return webSite;
    }

    public void setWebSite(WebSite webSite) {
        this.webSite = webSite;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public Long getPairingId() {
        return pairingId;
    }

    public void setPairingId(Long pairingId) {
        this.pairingId = pairingId;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
