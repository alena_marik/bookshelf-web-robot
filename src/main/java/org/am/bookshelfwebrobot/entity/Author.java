package org.am.bookshelfwebrobot.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = Author.TABLE_NAME)
public class Author extends BSWEntity {

    private final static String AUTHOR = "AUTHOR";
    protected final static String TABLE_NAME = BSW + AUTHOR;
    private final static String SEQUENCE_NAME = SEQ + TABLE_NAME;

    @SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "BIRTH_TIME")
    private String birthTime;

    @Column(name = "BIRTH_DATE")
    private LocalDate birthDate;

    @Column(name = "BIRTH_YEAR")
    private Integer birthYear;

    @Column(name = "DEATH_TIME")
    private String deathTime;

    @Column(name = "DEATH_DATE")
    private LocalDate deathDate;

    @Column(name = "DEATH_YEAR")
    private Integer deathYear;

    @Column(name = "BIRTH_PLACE")
    private String birthPlace;

    @Column(name = "COUNTRY")
    private String country;

    @Column(name = "WIKI_WEB_LINK")
    private String wikiWebLink;

    @Column(name = "CBDB_WEB_LINK")
    private String cbdbWebLink;

    @Column(name = "BOOK_DB_WEB_LINK")
    private String bookDBWebLink;

    @Column(name = "RATING")
    private Integer rating;

    @Lob
    @Column(name = "BIOGRAPHY")
    private String biography;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthTime() {
        return birthTime;
    }

    public void setBirthTime(String birthTime) {
        this.birthTime = birthTime;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(Integer birthYear) {
        this.birthYear = birthYear;
    }

    public String getDeathTime() {
        return deathTime;
    }

    public void setDeathTime(String deathTime) {
        this.deathTime = deathTime;
    }

    public LocalDate getDeathDate() {
        return deathDate;
    }

    public void setDeathDate(LocalDate deathDate) {
        this.deathDate = deathDate;
    }

    public Integer getDeathYear() {
        return deathYear;
    }

    public void setDeathYear(Integer deathYear) {
        this.deathYear = deathYear;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getWikiWebLink() {
        return wikiWebLink;
    }

    public void setWikiWebLink(String wikiWebLink) {
        this.wikiWebLink = wikiWebLink;
    }

    public String getCbdbWebLink() {
        return cbdbWebLink;
    }

    public void setCbdbWebLink(String cbdbWebLink) {
        this.cbdbWebLink = cbdbWebLink;
    }

    public String getBookDBWebLink() {
        return bookDBWebLink;
    }

    public void setBookDBWebLink(String bookDBWebLink) {
        this.bookDBWebLink = bookDBWebLink;
    }

    @Override
    public String toString() {
        return "AuthorData{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthTime='" + birthTime + '\'' +
                ", birthDate=" + birthDate +
                ", birthYear=" + birthYear +
                ", deathTime='" + deathTime + '\'' +
                ", deathDate=" + deathDate +
                ", deathYear=" + deathYear +
                ", birthPlace='" + birthPlace + '\'' +
                ", country='" + country + '\'' +
                ", wikiWebLink='" + wikiWebLink + '\'' +
                ", cbdbWebLink='" + cbdbWebLink + '\'' +
                ", bookDBWebLink='" + bookDBWebLink + '\'' +
                '}';
    }
}
