package org.am.bookshelfwebrobot.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = WebLink.TABLE_NAME)
public class WebLink extends BSWEntity{

    private final static String WEB_LINK = "WEB_LINK";
    protected final static String TABLE_NAME = BSW + WEB_LINK;
    private final static String SEQUENCE_NAME = SEQ + TABLE_NAME;

    @Id
    @Column(name = "WEB_LINK")
    private String url;

    @Column(name = "WEB_SITE")
    @Enumerated(EnumType.STRING)
    private WebSite webSite;

    @Column(name = "STATE")
    @Enumerated(EnumType.STRING)
    private State state;

    @Column(name = "AUTHOR_NAME")
    private String authorName;

    @Column(name = "ADDITIONAL_INFO")
    private String additionalInfo;

    @CreationTimestamp
    @Column(name = "CREATE_DATE", columnDefinition = "TIMESTAMP")
    private LocalDateTime createDate;

    @UpdateTimestamp
    @Column(name = "MODIFY_DATE", columnDefinition = "TIMESTAMP")
    private LocalDateTime modifyDate;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public WebSite getWebSite() {
        return webSite;
    }

    public void setWebSite(WebSite webSite) {
        this.webSite = webSite;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(LocalDateTime modifyDate) {
        this.modifyDate = modifyDate;
    }

}
