package org.am.bookshelfwebrobot.entity;

import javax.persistence.*;

@Entity
@Table(name = WebCrawlerState.TABLE_NAME)
public class WebCrawlerState extends BSWEntity{

    private final static String CRAWLER_STATE = "CRAWLER_STATE";
    protected final static String TABLE_NAME = BSW + CRAWLER_STATE;

    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "WEB_SITE")
    @Enumerated(EnumType.STRING)
    private WebSite webSite;

    @Column(name = "LETTER")
    private Character letter;

    @Column(name = "SORTING")
    private String sorting;

    @Column(name = "PAGE_NUMBER")
    private Integer pageNumber;

    @Column(name = "URL")
    private String url;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public WebSite getWebSite() {
        return webSite;
    }

    public void setWebSite(WebSite webSite) {
        this.webSite = webSite;
    }

    public Character getLetter() {
        return letter;
    }

    public void setLetter(Character letter) {
        this.letter = letter;
    }

    public String getSorting() {
        return sorting;
    }

    public void setSorting(String sorting) {
        this.sorting = sorting;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
