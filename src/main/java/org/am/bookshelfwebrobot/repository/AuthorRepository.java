package org.am.bookshelfwebrobot.repository;

import org.am.bookshelfwebrobot.entity.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface AuthorRepository extends JpaRepository<Author, Long> {

    Author findByCbdbWebLink(String cbdbWebLink);

    Author findByBookDBWebLink(String bookDBWebLink);

    Author findByWikiWebLink(String wikiWebLink);

    @Query(value = "select a.cbdbWebLink from Author a")
    Set<String> findCbdbWebLink();

    @Query(value = "select a.wikiWebLink from Author a")
    Set<String> findWikiWebLink();

    @Query(value = "select a.bookDBWebLink from Author a")
    Set<String> findBookDBWebLink();

}
