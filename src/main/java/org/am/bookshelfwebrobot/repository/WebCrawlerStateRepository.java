package org.am.bookshelfwebrobot.repository;

import org.am.bookshelfwebrobot.entity.WebCrawlerState;
import org.am.bookshelfwebrobot.entity.WebSite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WebCrawlerStateRepository extends JpaRepository<WebCrawlerState, Integer> {

    WebCrawlerState findByWebSite(WebSite webSite);

}