package org.am.bookshelfwebrobot.repository;

import org.am.bookshelfwebrobot.entity.State;
import org.am.bookshelfwebrobot.entity.WebLink;
import org.am.bookshelfwebrobot.entity.WebSite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface WebLinkRepository extends JpaRepository<WebLink, String> {

    @Query(value = "select w from WebLink w where w.createDate=(select max(w2.createDate) from WebLink w2 where w2.webSite=:webSite)")
    WebLink getLastestWebLinkByWebSite(@Param("webSite") WebSite webSite);

    List<WebLink> findByWebSite(WebSite webSite);

    List<WebLink> findByWebSiteAndState(WebSite webSite, State state);

    List<WebLink> findByState(State state);

    @Query(value = "select w.url from WebLink w where w.webSite=:webSite")
    Set<String> getWebLinks(@Param("webSite") WebSite webSite);

}
