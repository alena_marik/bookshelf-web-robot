package org.am.bookshelfwebrobot.repository;

import org.am.bookshelfwebrobot.entity.AuthorData;
import org.am.bookshelfwebrobot.entity.State;
import org.am.bookshelfwebrobot.entity.WebSite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface AuthorDataRepository extends JpaRepository<AuthorData, Long> {

    AuthorData findByUrl(String url);

    List<AuthorData> findByWebSiteAndState(WebSite webSite, State state);

    List<AuthorData> findByWebSiteAndStateAndLastName(WebSite webSite, State state, String lastName);

    List<AuthorData> findByState(State state);

    @Query(value = "select ad from AuthorData ad where ad.id=(select min(ad2.id) from AuthorData ad2 where ad2.state=:state)")
    AuthorData findNextByState(@Param("state")State state);

    @Query(value = "select ad.url from AuthorData ad where ad.webSite=:website")
    Set<String> findWebLinks(@Param("website") WebSite webSite);

    @Query(value = "select ad.url from AuthorData ad where ad.webSite='CBDB'")
    Set<String> findCbdbWebLink();

    @Query(value = "select ad.url from AuthorData ad where ad.webSite='WIKIPEDIA'")
    Set<String> findWikiWebLink();

    @Query(value = "select ad.url from AuthorData ad where ad.webSite='BOOK_DATABASE'")
    Set<String> findBookDBWebLink();

    @Query(value = "select max(ad.pairingId) from AuthorData ad")
    Long findMaxPairingId();

    AuthorData findByPairingIdAndWebSite(Long pairingId, WebSite webSite);

    @Query(value = "select ad from AuthorData ad where ad.webSite=:website and ad.pairingId=null")
    List<AuthorData> findByWebSiteAndNullPairingId(@Param("website") WebSite webSite);

    List<AuthorData> findByPairingId(Long pairingId);

    List<AuthorData> findByLastNameAndWebSite(String lastName, WebSite webSite);

}
