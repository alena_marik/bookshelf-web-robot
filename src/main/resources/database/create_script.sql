create table BSW_WEB_LINK(
WEB_LINK varchar2(100),
WEB_SITE varchar2(30),
STATE varchar2(30),
AUTHOR_NAME varchar2(100),
ADDITIONAL_INFO varchar2(500),
CREATE_DATE timestamp,
MODIFY_DATE timestamp,
CONSTRAINT PK_BSW_WEB_LINK PRIMARY KEY (WEB_LINK)
);

-----------------------------------------------------------------------------------

create table BSW_AUTHOR_DATA(
ID number(10) not null,
NAME varchar2(100),
FIRST_NAME varchar2(100),
LAST_NAME varchar2(100),
BIRTH_TIME varchar2(50),
BIRTH_DATE date,
BIRTH_YEAR number(10),
DEATH_TIME varchar2(50),
DEATH_DATE date,
DEATH_YEAR number(10),
BIRTH_PLACE varchar2(100),
COUNTRY varchar2(100),
URL varchar2(100),
WEB_SITE varchar2(30),
ADDITIONAL_INFO varchar2(500),
PAIRING_ID number(10),
STATE varchar2(30),
CONSTRAINT PK_BSW_AUTHOR_DATA PRIMARY KEY (ID)
);

CREATE SEQUENCE SEQ_BSW_AUTHOR_DATA
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

-----------------------------------------------------------------------------------

create table BSW_AUTHOR(
ID number(10) not null,
NAME varchar2(100),
FIRST_NAME varchar2(100),
LAST_NAME varchar2(100),
BIRTH_TIME varchar2(50),
BIRTH_DATE date,
BIRTH_YEAR number(10),
DEATH_TIME varchar2(50),
DEATH_DATE date,
DEATH_YEAR number(10),
BIRTH_PLACE varchar2(100),
COUNTRY varchar2(100),
WIKI_WEB_LINK varchar2(100),
CBDB_WEB_LINK varchar2(100),
BOOK_DB_WEB_LINK varchar2(100),
CONSTRAINT PK_BSW_AUTHOR PRIMARY KEY (ID)
);

CREATE SEQUENCE SEQ_BSW_AUTHOR
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

-----------------------------------------------------------------------------------

create table BSW_CRAWLER_STATE(
ID number(5),
WEB_SITE varchar2(30),
LETTER varchar2(1),
SORTING varchar2(100),
PAGE_NUMBER number(5),
URL varchar2(100),
CONSTRAINT PK_BSW_CRAWLER_STATE PRIMARY KEY (ID)
);

insert into BSW_CRAWLER_STATE values(1, 'BOOK_DATABASE', NULL, NULL, NULL, NULL);
insert into BSW_CRAWLER_STATE values(2, 'CBDB', 'a', NULL, 1, NULL);
insert into BSW_CRAWLER_STATE values(3, 'WIKIPEDIA', NULL, NULL, NULL, NULL);

commit;

-----------------------------------------------------------------------------------